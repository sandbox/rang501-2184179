<?php
/**
 * @file
 * Exports strings.
 */

/**
 * Implements hook_init().
 */
function export_frontend_strings_init() {
  // Ignore admin paths.
  if (!path_is_admin(current_path()) && !drupal_is_cli()) {
    $strings = &drupal_static('locale');
    $system_strings = &drupal_static(__FUNCTION__);
    // Save current state.
    $system_strings = $strings;
  }
}

/**
 * Implements hook_menu().
 */
function export_frontend_strings_menu() {
  $items = array();

  $items['admin/config/regional/csv-translation'] = array(
    'title' => 'Frontend translations',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('export_frontend_strings_import_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'export_frontend_strings.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/config/regional/csv-translation/download'] = array(
    'title' => 'Download translations',
    'page callback' => 'export_frontend_strings_download',
    'page arguments' => array(),
    'access arguments' => array('administer site configuration'),
    'file' => 'export_frontend_strings.admin.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_preprocess_HOOK().
 */
function export_frontend_strings_preprocess_html(&$variables) {
  global $language;

  // Run only on non-admin paths.
  if (!path_is_admin(current_path()) && !drupal_is_cli()) {
    $js = &drupal_static('drupal_add_js', array());
    $strings = &drupal_static('locale');
    $system_strings = &drupal_static('export_frontend_strings_init');

    $all = $strings[$language->language][''];
    $initial = $system_strings[$language->language][''];
    $diff = array_diff($all, $initial);

    $merge = array_keys($diff);

    if (!is_array($merge)) {
      $merge = array();
    }

    _export_frontend_strings_merge($merge);

    $js_keys = array_keys($js);

    foreach ($js_keys as $path) {
      _export_frontend_strings_from_db($path);
    }
  }
}

/**
 * Find strings by path. Used for finding strings for javascript files.
 *
 * @param string $location
 *   Path to page or javascript file.
 */
function _export_frontend_strings_from_db($location) {
  if (!path_is_admin(current_path()) && !drupal_is_cli()) {
    $query = db_select('locales_source', 'ls');
    $query->fields('ls');
    $query->condition('ls.location', $location);
    $result = $query->execute();
    $strings = array();

    while ($row = $result->fetchAssoc()) {
      $strings[] = $row['source'];
    }

    _export_frontend_strings_merge($strings);
  }

}

/**
 * Process strings.
 *
 * @param array $strings
 *   Array of strings to process.
 */
function _export_frontend_strings_merge(array $strings) {
  $collected = variable_get('export_frontend_strings_collected', array());
  $excluded = variable_get('export_frontend_strings_exclude', array());

  if (is_array($strings)) {
    $collected = array_merge($collected, $strings);
  }

  if (is_array($excluded)) {
    $collected = array_diff($collected, $excluded);
  }

  $collected = array_unique($collected);

  variable_set('export_frontend_strings_collected', $collected);
}
