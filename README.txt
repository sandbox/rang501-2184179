INTRODUCTION
------------
This module helps to collect useful translations.
Common use case is to collect all front end translations while skipping
all backend or unrelated translations (there are thousands of strings).
You basically enable this module and start navigating around Your site.
Do not use this module on production site!

INSTALLATION
------------
 * Enable module
 * Navigate around


CSV import
----------
This module adds support for CSV translations import.
First line must contain language codes.
First column contain source strings.
