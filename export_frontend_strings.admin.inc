<?php
/**
 * @file
 * Administrative pages.
 */

/**
 * CSV import page form.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 *
 * @return array
 *   Form elements array.
 */
function export_frontend_strings_import_form(array $form, array &$form_state) {

  $collected = variable_get('export_frontend_strings_collected', array());
  $collected_count = count($collected);

  $form['csv_file_download'] = array(
    '#type' => 'fieldset',
    '#title' => t('CSV File download'),
  );

  $form['csv_file_download']['download_markup'] = array(
    '#markup' => '<p>' . t('Strings collected: @count', array('@count' => $collected_count)) . '</p>' . l(t('Download'), 'admin/config/regional/csv-translation/download'),
  );

  $form['csv_file_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('File upload'),
  );

  $form['csv_file_fieldset']['cvs_upload'] = array(
    '#type' => 'managed_file',
    '#title' => t('CSV file'),
    '#upload_validators' => array('file_validate_extensions' => array('csv')),
  );

  $form['upload'] = array(
    '#type' => 'submit',
    '#value' => t('Upload and import'),
  );

  return $form;
}

/**
 * Download file callback.
 */
function export_frontend_strings_download() {
  drupal_add_http_header('Content-Disposition', 'attachment; filename="exported_strings.csv"');
  drupal_add_http_header('Content-Transfer-Encoding', 'binary"');
  drupal_add_http_header('Pragma', 'no-cache"');
  drupal_add_http_header('Expires', '0');
  drupal_add_http_header('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');

  $csv = variable_get('export_frontend_strings_collected', array());
  $out = fopen('php://output', 'w');

  fputcsv($out, array('en'));

  foreach ($csv as $line) {
    $s = array(
      $line,
    );

    fputcsv($out, $s);
  }

  fclose($out);
  drupal_exit();
}

/**
 * CSV upload form submit handler.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function export_frontend_strings_import_form_submit(array $form, array $form_state) {
  $file = file_save_upload('cvs_upload');
  $f = fopen($file->uri, 'r');
  $overrides = variable_get('locale_custom_strings_en');

  // Find language codes from first line.
  $language_list = fgetcsv($f);
  // Remove first column because this is the source string column.
  unset($language_list[0]);

  while ($line = fgetcsv($f)) {

    foreach ($language_list as $key => $lang) {

      if ($lang == 'en') {

        if (!empty($line[$key])) {
          $overrides[''][$line[0]] = $line[$key];
        }

      }
      else {

        if (!empty($line[$key])) {
          export_frontend_strings_add_translation($line[0], $line[$key], $lang);
        }

      }

    }

  }

  if (!empty($overrides)) {
    variable_set('locale_custom_strings_en', $overrides);
  }

  drupal_set_message(t('Translations updated!'));
}

/**
 * Wrapper function to import translations.
 *
 * @param string $source
 *   Source string.
 * @param string $translation
 *   Translation form source string.
 * @param string $langcode
 *   Translation language code.
 */
function export_frontend_strings_add_translation($source, $translation, $langcode) {
  $report = array(
    'skips' => 0,
    'updates' => 0,
    'deletes' => 0,
    'additions' => 0,
  );

  $textgroup = 'default';
  $mode = LOCALE_IMPORT_OVERWRITE;
  $location = '';
  $context = '';

  _export_frontend_strings_import_one_string_db($report, $langcode, $context, $source, $translation, $textgroup, $location, $mode);
}


/**
 * Import one string into the database.
 *
 * @param array $report
 *   Report array summarizing the number of changes done in the form:
 *   array(inserts, updates, deletes).
 * @param string $langcode
 *   Language code to import string into.
 * @param string $context
 *   The context of this string.
 * @param string $source
 *   Source string.
 * @param string $translation
 *   Translation to language specified in $langcode.
 * @param string $textgroup
 *   Name of textgroup to store translation in.
 * @param string $location
 *   Location value to save with source string.
 * @param int $mode
 *   Import mode to use, LOCALE_IMPORT_KEEP or LOCALE_IMPORT_OVERWRITE.
 * @param int $plid
 *   Optional plural ID to use.
 * @param int $plural
 *   Optional plural value to use.
 *
 * @return int
 *   The string ID of the existing string modified or the new string added.
 */
function _export_frontend_strings_import_one_string_db(array &$report, $langcode, $context, $source, $translation, $textgroup, $location, $mode, $plid = 0, $plural = 0) {
  $lid = db_query("SELECT lid FROM {locales_source}
  WHERE source = :source AND context = :context AND textgroup = :textgroup",
    array(
      ':source' => $source,
      ':context' => $context,
      ':textgroup' => $textgroup,
    ))->fetchField();

  if (!empty($translation)) {
    // Skip this string unless it passes a check for dangerous code.
    // Text groups other than default still can contain HTML tags
    // (i.e. translatable blocks).
    if ($textgroup == "default" && !locale_string_is_safe($translation)) {
      $report['skips']++;
      $lid = 0;
    }
    elseif ($lid) {
      // We have this source string saved already.
      db_update('locales_source')
        ->fields(array(
          'location' => $location,
        ))
        ->condition('lid', $lid)
        ->execute();

      $exists = db_query("SELECT COUNT(lid) FROM {locales_target} WHERE lid = :lid AND language = :language", array(':lid' => $lid, ':language' => $langcode))->fetchField();

      if (!$exists) {
        // No translation in this language.
        db_insert('locales_target')
          ->fields(array(
            'lid' => $lid,
            'language' => $langcode,
            'translation' => $translation,
            'plid' => $plid,
            'plural' => $plural,
          ))
          ->execute();

        $report['additions']++;
      }
      elseif ($mode == LOCALE_IMPORT_OVERWRITE) {
        // Translation exists, only overwrite if instructed.
        db_update('locales_target')
          ->fields(array(
            'translation' => $translation,
            'plid' => $plid,
            'plural' => $plural,
          ))
          ->condition('language', $langcode)
          ->condition('lid', $lid)
          ->execute();

        $report['updates']++;
      }
    }
    else {
      // No such source string in the database yet.
      $lid = db_insert('locales_source')
        ->fields(array(
          'location' => $location,
          'source' => $source,
          'context' => (string) $context,
          'textgroup' => $textgroup,
        ))
        ->execute();

      db_insert('locales_target')
        ->fields(array(
          'lid' => $lid,
          'language' => $langcode,
          'translation' => $translation,
          'plid' => $plid,
          'plural' => $plural,
        ))
        ->execute();

      $report['additions']++;
    }
  }
  elseif ($mode == LOCALE_IMPORT_OVERWRITE) {
    // Empty translation, remove existing if instructed.
    db_delete('locales_target')
      ->condition('language', $langcode)
      ->condition('lid', $lid)
      ->condition('plid', $plid)
      ->condition('plural', $plural)
      ->execute();

    $report['deletes']++;
  }

  return $lid;
}
